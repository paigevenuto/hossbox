from flask import Blueprint, render_template, redirect, session, jsonify, request
from credsrequired import creds_required
from uuid import uuid4
import time
import sys
sys.path.insert(1, '../')
from esd import esdBulkHostnameSearch

decom_blueprint = Blueprint('decom', __name__)

"""
Decom Dashboard view
"""
@decom_blueprint.route('/decom')
@creds_required
def decom():
    return render_template('decom.html')

"""
Route for sending new tasks to app
"""
@decom_blueprint.route('/decom-tasks', methods=['POST'])
def newDecomTask():
    if 'new-task-hostnames' not in request.form.keys():
        return redirect('/decom')

    # TODO
    task_id = uuid4()
    # UTC epoch, gets decoded on frontend
    timestamp = int( time.time() * 1000 )
    task_name = request.form['new-task-name']
    hostnames = request.form['new-task-hostnames'].splitlines()
    pid_user = session['charter_creds']['pid_user']
    adm_user = session['charter_creds']['adm_user']

    servers = esdBulkHostnameSearch(hostnames)

    task = {
            'id': task_id,
            'timestamp': timestamp,
            'name': task_name,
            'status': 'pending',
            'pid': pid_user,
            'adm': adm_user,
            'servers': servers
            }
    print(task)
    return jsonify(task)

dummydata = {"adm":"test","id":"b9e2525b-c232-436d-8546-2366d248ccba","name":"blah","pid":"test","servers":[{"email":"DL-EDC-Windows@charter.com","esd":"https://esd.twcable.com/object/view/hw_asset?id=181221","hostname":"wopspapp13","ip":"10.64.208.34","oob":"https://vrops.chartercom.com/","owner":"Stephen White ( Stephen.White@charter.com )","vendor":"VMware", "status":"checking power"}],"status":"pending","timestamp":1633717481670}



"""
Route sends a json list of all tasks for user
"""
@decom_blueprint.route('/decom-tasks', methods=['GET'])
def decomTasks():
    # TODO
    return jsonify( [dummydata ] )

"""
Route deletes a list of tasks from a user
"""
@decom_blueprint.route('/decom-tasks', methods=['DELETE'])
def deleteDecomTasks():
    # TODO
    pid_user = session['charter_creds']['pid_user']
    adm_user = session['charter_creds']['adm_user']
    tasksList = request.json
    return 'Tasks deleted'


