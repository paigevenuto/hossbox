from flask import Blueprint, render_template, redirect, session, jsonify, request
from esd import esdBulkHostnameSearch
import json

getemails_blueprint = Blueprint('getemails', __name__)

"""
GetEmails Dashboard view
"""
@getemails_blueprint.route('/getemails', methods=['GET'])
def getemails():
    return render_template('getemails.html')

"""
Route for getting emails from hostnames
"""
@getemails_blueprint.route('/getemails', methods=['POST'])
def newGetEmailsTask():
    hostnames = request.json
    # removes blank lines
    hostnames = [hostname for hostname in hostnames if hostname != '']
    esdResults = esdBulkHostnameSearch(hostnames)
    # removes data that isn't emails
    emails = list( map( lambda x: x['support'], esdResults ))
    # this seems redundant but it is because some strings in the array have
    # commas separating more than one email
    emails = '; '.join(emails)
    emails = emails.split('; ')
    # this removes duplicate emails
    emails = list(set(emails))
    # separates the emails with semicolons for outlook
    emails = '; \n'.join(emails)
    # includes oracle DB team if oracle server
    if 'ORA' in "".join(hostnames):
        emails = emails + "; \nDL-Enterprise-OracleAdmins@charter.com"
    return json.dumps(emails)
