from functools import wraps
from flask import session, redirect
import yaml
with open('config.yml', 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)
TESTING = cfg['testing']

def creds_required(function):
    @wraps(function)
    def wrapper(*args, **kwargs): 
        """
        Only allows access to route if creds are entered
        """
        if TESTING:
            return function()
        try:
            creds = session['charter_creds']
            return function()
        except:
            return redirect('/creds')
    return wrapper
