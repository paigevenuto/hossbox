from flask import Blueprint, render_template, redirect, session, jsonify, request
from esd import esdBulkHostnameSearch
import json

contactinfo_blueprint = Blueprint('contactinfo', __name__)

"""
ContactInfo Dashboard view
"""
@contactinfo_blueprint.route('/contactinfo', methods=['GET'])
def contactinfo():
    return render_template('contactinfo.html')

"""
Route for getting emails from hostnames
"""
@contactinfo_blueprint.route('/contactinfo', methods=['POST'])
def newContactInfoTask():
    hostnames = request.json
    # removes blank lines
    hostnames = [hostname for hostname in hostnames if hostname != '']
    esdResults = esdBulkHostnameSearch(hostnames)
    return json.dumps(esdResults)
