from flask import Blueprint, render_template, redirect, session, jsonify, request
import sys
sys.path.insert(1, '../')
from nmap import batchServiceScan
import json


services_blueprint = Blueprint('services', __name__)

"""
GetEmails Dashboard view
"""
@services_blueprint.route('/services', methods=['GET'])
def services():
    return render_template('services.html')

@services_blueprint.route('/services', methods=['POST'])
def getServices():
    hostnames = request.json
    hostnames = [hostname for hostname in hostnames if hostname != '']
    results = batchServiceScan(hostnames)
    print(json.dumps(results))
    return json.dumps(results)
