from flask import Blueprint, session, request, render_template, flash
import sys
sys.path.insert(1, '../scripts')
try:
    from verifyCredentials import verifyCredentials
except:
    print('This app can only run on windows machines')

creds_blueprint = Blueprint('creds', __name__)

@creds_blueprint.route('/creds', methods=['GET', 'POST'])
def creds():
    blank_creds = { 'pid_user':'', 'pid_pass':'', 'adm_user':'', 'adm_pass':'' }

    # If credentials aren't completely filled out
    if len(request.form) < 4 or '' in request.form.values():
        flash( { 'text': 'Please enter all credentials', 'type': 'danger' } )
        charter_creds = session['charter_creds'] if'charter_creds' in session.keys() else blank_creds

    else:
        pid_user = request.form['pid_user']
        pid_pass = request.form['pid_pass']
        adm_user = request.form['adm_user']
        adm_pass = request.form['adm_pass']
        charter_creds = {
                'pid_user':pid_user,
                'pid_pass':pid_pass,
                'adm_user':adm_user,
                'adm_pass':adm_pass,
                }

        # If credentials are valid
        if verifyCredentials(pid_user, pid_pass) and verifyCredentials(adm_user, adm_pass):
            session['charter_creds'] = charter_creds
            flash( { 'text': 'Credentials updated successfully.', 'type': 'success' } )

        # If credentials are invalid
        else:
            flash( { 'text': 'Credentials invalid', 'type': 'danger' } )
            charter_creds = session['charter_creds'] if'charter_creds' in session.keys() else blank_creds
    return render_template('creds.html', creds=charter_creds)
