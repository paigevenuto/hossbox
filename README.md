# HOSSbox

![HOSSbox icon](./static/images/hossbox.png)

## Quickstart

1. Install python requirements. `pip install -r requirements.txt`
2. Install RabbitMQ for task broker. [RabbitMQ download](https://www.rabbitmq.com/download.html)
3. Install database.
4. Create and configure `config.yml` (template can be found below)
5. Run using `python app.py`

## Features

### Automate decommisions and report (WIP)

![Decom tool screenshot](./readme/decom.png)

Overview

- Accepts a list of hostnames as input
- Checks power status of each device
- Sends shutdown command using Out-of-Band service
- Confirms shutdown was successful

![Decom confirmation dialog screenshot](./readme/decom-check.png)

Responsible

- Database logs which user created each task
- Displays confirmation dialog before execution
- Prevents use on more than 50 servers per task

### Aggregate emails from ESD

Overview

- Accepts a list of hostnames as input
- Returns list of owner emails from ESD
- Uses colon-separated values for compatibility with Outlook

![Emails tool screenshot](./readme/email.png)

### Credentials saved securely

Overview

- Credentials are stored in an encrypted HTTP-only cookie
- Cookie is only saved for the duration of the session
- Credentials are not stored in a database
- Submitted credentials are verified to ensure authenticity

![Credentials page screenshot](./readme/creds.png)

### Service scan (WIP)

Overview

- Accepts a list of hostnames as input
- Checks each hostname for services running using nmap backend

![Services page screenshot](./readme/services.png)

### Audit scan (WIP)

Overview

- Accepts a list of hostnames as input
- Checks for compliance to audit requirements

## Configuration

    port:
    database_url:
    secret_key:
    idrac_user:
    idrac_pass:
    ilo_credslist:
      - username:
        password:
      - username:
        password:
    oracle_VM_user:
    oracle_VM_pass:
    esd_key:
    esd_eid:
    testing:
