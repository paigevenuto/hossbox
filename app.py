from waitress import serve
from flask import Flask
from models import db, connect_db
import datetime
import json
import yaml
import sys
sys.path.insert(1, './routes')
sys.path.insert(1, './scripts')
from ilo import iloCheckPoweredOn
from idrac import idracCheckPoweredOn
from vsphere import vsphereCheckPoweredOn

app = Flask(__name__)

"""
Settings for the app are found in the config file
"""
with open('config.yml', 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)
PORT = cfg['port']
app.config['SECRET_KEY'] = cfg['secret_key']
TEMPLATES_AUTO_RELOAD = True
DATABASE_URL = cfg['database_url']
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True
SESSION_COOKIE_SAMESITE = 'Strict'

# Safety check to prevent security issue
# if app.config['SECRET_KEY'] == 'change_me':
    # sys.exit('User credentials are encrypted using SECRET_KEY!\nPlease change this value in the config file!')

"""
These are all the routes that the app serves
They are in the routes folder to keep this app tidy and modular
"""

from index import index_blueprint
app.register_blueprint(index_blueprint)

from creds import creds_blueprint
app.register_blueprint(creds_blueprint)

from decom import decom_blueprint
app.register_blueprint(decom_blueprint)

from getemails import getemails_blueprint
app.register_blueprint(getemails_blueprint)

from services import services_blueprint
app.register_blueprint(services_blueprint)

from contactinfo import contactinfo_blueprint
app.register_blueprint(contactinfo_blueprint)

"""
This app is written with Flask, was served using waitress
Now it is served using IIS
"""
print(f"Running server on port {PORT}")
# serve(app, port=PORT)
