document.addEventListener("DOMContentLoaded", () => {
  /*
   *Event for when the check all button is clicked
   */
  $("#getservices").on("click", () => {
    const hostnames = $("#hostnames").val().split("\n");
    $("#services-accordion").html("");
    getServersServices(hostnames);
  });

  /*
   *Function for getting server services from API
   */
  async function getServersServices(hostnames) {
    const response = await axios.post("./services", hostnames);
    const servers = response.data;
    servers.forEach((server) => addServerToAccordion(server));
  }

  /*
   *Creates accordion item from template and populates using createServiceItem
   */
  function addServerToAccordion(server) {
    let accordionItem = document.getElementById("accordion-item-template");
    accordionItem = accordionItem.content.cloneNode(true);
    $(accordionItem)
      .find("button")
      .attr("data-bs-target", `#${server.hostname}`);
    $(accordionItem).find(".accordion-collapse").attr("id", server.hostname);
    $(accordionItem).find(".accordion-item-label").text(server.hostname);
    if (server.services.length != 0) {
      $(accordionItem).find(".badge").text(server.services.length);
      $(accordionItem).find(".badge").removeAttr("hidden");
      $(accordionItem).find(".fas").addClass("fa-times-circle");
      $(accordionItem).find(".fas").addClass("text-danger");
    } else {
      $(accordionItem).find(".fas").addClass("fa-check-square");
      $(accordionItem).find(".fas").addClass("text-success");
    }

    server.services.forEach((service) =>
      $(accordionItem).find("ul").append(createServiceItem(service))
    );
    $("#services-accordion").append(accordionItem);
  }

  /*
   *Creates service item using service object from API
   */
  function createServiceItem(service) {
    let serviceItem = document.getElementById("service-item-template");
    serviceItem = serviceItem.content.cloneNode(true);
    $(serviceItem).find(".version").text(service.version);
    $(serviceItem).find(".state").text(service.state);
    $(serviceItem).find(".port").text(`${service.service} (${service.port})`);

    return serviceItem;
  }
});

