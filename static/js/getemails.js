document.addEventListener("DOMContentLoaded", () => {
  $("#getemails").on("click", () => {
    const hostnames = $("#hostnames").val().split("\n");
    getBulkEmails(hostnames);
  });

  async function getBulkEmails(hostnames) {
    const response = await axios.post("./getemails", hostnames);
    const emails = response.data;
    $("#emails").val(emails);
  }
});

