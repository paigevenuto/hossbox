document.addEventListener("DOMContentLoaded", () => {
  $("#getcontactinfo").on("click", () => {
    const hostnames = $("#hostnames").val().split("\n");
    getBulkEmails(hostnames);
    $(".left-pane").remove();
    $(".right-pane").toggleClass("col-9");
    $(".right-pane").toggleClass("col-12");
  });

  async function getBulkEmails(hostnames) {
    const response = await axios.post("./contactinfo", hostnames);
    const contactinfo = response.data;
    $("#contactinfo-body").val(contactinfo);
    for (server of contactinfo) {
      const tr = document.createElement("tr");
      const details = [
        server.hostname,
        server.support.split("; ").join(";\n"),
        server.owner,
        server.techexec,
        server.sponsor,
        server.notificationdl,
        server.dlcontacts,
      ];
      for (detail of details) {
        const td = document.createElement("td");
        td.innerText = detail;
        tr.append(td);
      }
      $("#contactinfo-body").append(tr);
    }
  }
});

