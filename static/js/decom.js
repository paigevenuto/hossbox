document.addEventListener("DOMContentLoaded", () => {
  /*
   *Automatically requests and loads tasks from /decom-tasks on load
   */
  let tasks = [];
  async function getTasks() {
    let request = await axios.get("/decom-tasks");
    tasks = request.data;
    tasks.forEach((task) => makeNewTask(task));
    $(".task")[0].click();
  }
  getTasks();

  /*
   *Function to populate task details pane
   */
  const showTaskDetails = (task_id) => {
    const taskDetailsBody = $("#task-details-body");
    taskDetailsBody.html("");
    const currentTask = tasks.find((task) => task.id == task_id);
    for (server of currentTask.servers) {
      const tr = document.createElement("tr");
      const taskDetails = [
        server.hostname,
        server.vendor,
        server.oob,
        server.esd,
        server.status,
        server.ip,
      ];
      for (detail of taskDetails) {
        const td = document.createElement("td");
        if (detail.includes("://")) {
          const a = document.createElement("a");
          a.setAttribute("href", detail);
          a.innerText = detail.split("/")[2];
          td.append(a);
        } else {
          td.innerText = detail;
        }
        tr.append(td);
      }
      taskDetailsBody.append(tr);
    }
  };

  /*
   *Function for when a task on the list is clicked
   */
  const makeActiveTask = (activeTask) => {
    const allTasks = $(".task");
    allTasks.each((i, task) => {
      $(task).attr("aria-curent", null);
      task.classList.remove("active");
    });
    if (activeTask.tagName != "a") {
      $(activeTask).attr("aria-curent", "true");
      activeTask = activeTask.closest(".task");
    }
    activeTask.classList.add("active");
    task_id = $(activeTask).attr("id");
    showTaskDetails(task_id);
  };

  /*
   *Event for when a task on the list is clicked
   */
  $(document).on("click", ".task", (evt) => {
    if (evt.target.tagName != "INPUT") {
      makeActiveTask(evt.target);
    }
  });

  /*
   *Makes cursor go to name input automatically when modal opens
   */
  $("#new-task-modal").on("shown.bs.modal", () => {
    $("#new-task-name").focus();
  });

  /*
   *Function for adding new tasks to the list
   */
  const makeNewTask = (task) => {
    let taskItem = document.getElementById("task-item-template");
    taskItem = taskItem.content.cloneNode(true);
    $(taskItem).find(".task").attr("id", task.id);
    $(taskItem).find(".task-name").text(`Task: ${task.name}`);
    $(taskItem).find(".task-status").text(`Status: ${task.status}`);
    const taskTime = new Date(task.timestamp);
    const timeNow = Date.now();
    const timeDiff = timeNow - taskTime;
    let timeStatus = Math.floor(timeDiff / 1000 / 60);
    if (timeStatus < 60) {
      timeStatus = `${timeStatus} min ago`;
    } else {
      timeStatus = Math.floor(timeStatus / 60);
      timeStatus = `${timeStatus} hr ago`;
    }
    const diffInMinutes = new Date(timeDiff).getMinutes();
    $(taskItem).find(".task-time").text(timeStatus);
    $("#task-list").prepend(taskItem);
  };

  /*
   *Deletes selected tasks when clear is clicked
   */
  $("#task-clear-btn").on("click", () => {
    const task_ids = [];
    $(".task").each((i, task) => {
      if ($(task).find(":checked").length == 1) {
        const task_id = $(task).attr("id");
        task_ids.push(task_id);
        $(task).remove();
      }
    });
    axios.delete("/decom-tasks", { data: task_ids });
    const taskDetailsBody = $("#task-details-body");
    taskDetailsBody.html("");
  });

  /*
   *Copies table contents to clipboard
   */
  $("#task-details-copy").on("click", () => {
    const taskTable = document.querySelector("#task-details-body");
    var body = document.body,
      range,
      sel;
    if (document.createRange && window.getSelection) {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      try {
        range.selectNodeContents(taskTable);
        sel.addRange(range);
      } catch (e) {
        range.selectNode(taskTable);
        sel.addRange(range);
      }
    } else if (body.createTextRange) {
      range = body.createTextRange();
      range.moveToElementText(taskTable);
      range.select();
    }
    document.execCommand("copy");
    $("#task-details-copy").text("Copied!");
    setTimeout(() => {
      $("#task-details-copy").text("Copy Table");
    }, 1000);
  });

  /*
   *Populates the number of servers form confirmation dialog
   */

  let numServers = 0;
  $("#new-task-form-btn").on("click", () => {
    numServers = $("#new-task-hostnames")
      .val()
      .split("\n")
      .filter((x) => x != "").length;
    const newTaskModal = bootstrap.Modal.getOrCreateInstance(
      $("#new-task-modal")
    );
    const confirmModal = bootstrap.Modal.getOrCreateInstance(
      $("#confirmation-modal")
    );
    $("#num-of-svrs").text(numServers);
    if ($("#new-task-name").val() != "" && numServers != 0) {
      newTaskModal.hide();
      confirmModal.show();
    }
  });

  /*
   * Submits the form when confirmation is correct
   */

  $("#new-task-submit").on("click", () => {
    const numServersConfirm = $("#num-of-svrs-confirm");
    if (numServers == numServersConfirm.val()) {
      $("#new-task-form").submit();
    } else {
      numServersConfirm.addClass("is-invalid");
      $("#invalid-number").removeAttr("hidden");
    }
  });
});

