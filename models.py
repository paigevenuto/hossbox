from flask_sqlalchemy import SQLAlchemy
import datetime

db = SQLAlchemy()

def connect_db(app):
    """Connect to database"""

    db.app = app
    db.init_app(app)

# class Decomtask(db.Model):
    # """DecomTask."""

    # __tablename__ = "decom_tasks"

    # id = db.Column(db.CHAR(36), primary_key=True, nullable=False)
    # pid = db.Column(db.CHAR(8), nullable=False)
    # adm = db.Column(db.CHAR(11), nullable=False)
    # status = db.Column(db.VARCHAR(30), nullable=False)
    # name = db.Column(db.VARCHAR(30), nullable=False)
    # epoch = db.Column(db.Integer, nullable=False)

# class DecomServers(db.Model):
    # """DecomServers."""

    # __tablename__ = "decom_servers"

    # task_id = db.Column(db.CHAR(36), db.ForeignKey('decom_tasks.id'))
    # hostname = db.Column(db.VARCHAR(15), nullable=False)
    # ip = db.Column(db.CHAR(15), nullable=False)
    # vendor = db.Column(db.VARCHAR(10), nullable=False)
    # oob = db.Column(db.VARCHAR(255), nullable=False)
    # esd = db.Column(db.VARCHAR(60), nullable=False)
    # status = db.Column(db.VARCHAR(30), nullable=False)
    # first_powerstatus = db.Column(db.VARCHAR(3), nullable=False)
    # second_powerstatus = db.Column(db.VARCHAR(3), nullable=False)
