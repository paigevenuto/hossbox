CREATE TABLE decom_tasks
(
    id CHAR(36) PRIMARY KEY NOT NULL,
    pid CHAR(8) NOT NULL,
    adm CHAR(11) NOT NULL,
    status VARCHAR(30) NOT NULL,
    name VARCHAR(30) NOT NULL,
    epoch INTEGER NOT NULL,
    epoch_time as dateadd(s, epoch, '19700101')
)

CREATE TABLE decom_servers
(
    task_id CHAR(36) REFERENCES decom_tasks (id) ON DELETE CASCADE,
    hostname VARCHAR(15) NOT NULL,
    ip CHAR(15) NOT NULL,
    vendor VARCHAR(10) NOT NULL,
    oob VARCHAR(255) NOT NULL,
    esd VARCHAR(60) NOT NULL,
    status VARCHAR(30) NOT NULL,
    first_powerstatus VARCHAR(3) NOT NULL,
    second_powerstatus VARCHAR(3) NOT NULL
)

CREATE TABLE service_scan_tasks
(
    id CHAR(36) PRIMARY KEY NOT NULL,
    pid CHAR(8) NOT NULL,
    adm CHAR(11) NOT NULL,
    epoch INTEGER NOT NULL,
    epoch_time as dateadd(s, epoch, '19700101')
)

CREATE TABLE service_scan_hostnames
(
    id SERIAL PRIMARY KEY,
    task_id CHAR(36) REFERENCES service_scan_tasks (id) ON DELETE CASCADE,
    port INTEGER NOT NULL,
    service VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    version VARCHAR 255 NOT NULL
)
