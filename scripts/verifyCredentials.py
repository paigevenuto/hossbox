import win32security

def verifyCredentials(username, password):
    domain = 'corp.chartercom.com'
    try:
      hUser = win32security.LogonUser (
        username,
        domain,
        password,
        win32security.LOGON32_LOGON_NETWORK,
        win32security.LOGON32_PROVIDER_DEFAULT
      )
    except win32security.error:
        return False
    return True
