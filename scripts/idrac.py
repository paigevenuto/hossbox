import requests
import json
import yaml
import sys
sys.path.insert(1, '../')

with open('config.yml', 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)
idrac_user = cfg['idrac_user']
idrac_pass = cfg['idrac_pass']

def idracCheckPoweredOn(srvIp):

    r = requests.get(f"https://{srvIp}/redfish/v1/Systems/System.Embedded.1", verify=False, auth=(idrac_user, idrac_pass))
    powerState = r.json()['PowerState']
    if powerState == 'On':
        return True
    elif powerState == 'Off':
        return False
    else:
        return 'incorrect IP address entered or unknown iDRAC credentials'


def idracShutdown(srvIp):
    r = requests.post(f"https://{srvIp}/redfish/v1/Systems/System.Embedded.1/Actions/ComputerSystem.Reset", verify=False, auth=(idrac_user, idrac_pass), data={'ResetType': 'Off'})
    return r
