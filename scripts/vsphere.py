try:
    from pyVim.connect import SmartConnectNoSSL
except ImportError:
    from pyvim.connect import SmartConnectNoSSL

# example for testing
# must get from ESD unfortunately
vcenter = 'd06nutlp0002.twcable.com'
svr = 'vm0dnoravsd0013'

def vsphereCheckPoweredOn(svr, vcenter, admuser, admpass):
    try:
        c = SmartConnectNoSSL(host=vcenter, user=admuser, pwd=admpass, port=443)
        vm = c.content.searchIndex.FindByDnsName(dnsName=svr, vmSearch=True)
        powerStatus = vm.runtime.powerState
        if powerStatus == 'poweredOn':
            return True
        elif powerStatus == 'poweredOff':
            return False
    except:
        return 'hostname not found'

def vsphereShutdown(svr, vcenter, admuser, admpass):
    try:
        c = SmartConnectNoSSL(host=vcenter, user=admuser, pwd=admpass, port=443)
        vm = c.content.searchIndex.FindByDnsName(dnsName=svr, vmSearch=True)
        vm.ShutDownGuest()
        return 'shutdown successful'
    except:
        return 'hostname not found'
