import yaml
import requests
import sys
import json
import re
from threading import Thread
sys.path.insert(1, '../')

with open('config.yml', 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)
esd_key = cfg['esd_key']
esd_eid = cfg['esd_eid']

def stripEmails(emails):
    output = re.findall('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', emails)
    return '; '.join(output)

def esdFormatResults(results):
    support, dl_contacts = esdContactSearch(results['Application::ID'])
    return {
            'hostname': results['Hostname'],
            'dlcontacts': dl_contacts,
            'owner': results['Application::Application Owner'],
            'vendor': results['Hardware::Vendor'],
            'oob': results['Out-of-Band Info'],
            'ip': results['Primary IP(IP Only)'],
            'esd': "https://esd.twcable.com/object/view/hw_asset?id=" + results['ID'],
            'status': 'checking power',
            "support": support,
            "owner": stripEmails( results['Application::Application Owner'] ),
            "techexec": stripEmails( results['Application::Technology Executive/VP'] ),
            "sponsor": stripEmails( results['Application::Business Sponsor'] ),
            "notificationdl": stripEmails( results['Application::Notification DL'] )
            }


def esdParseResults(results):
    count = len(results['fields'].values())
    output = {}
    for i in range(0, count):
        field = results['fields'][str(i)]
        result = results['results']['0'][str(i)]
        output[field] = result
    return output

def esdHostnameSearch(hostname):
    search_body = {
            "auth": {
                "eid": esd_eid,
                "api_key": esd_key
                },
            "call": {
                "table": "hw_asset",
                "fields": {
                    "hostname": {
                        "hide": "0",
                        "sort": "1",
                        "sort_dir": "1"
                        },
                    "application": {
                        "hide": "0",
                        "fields": {
                            "appcontact": {
                                "hide": "0"
                                },
                            "appadmin": {
                                "hide": "0"
                                },
                            "tsgowner": {
                                "hide": "0"
                                },
                            "owner": {
                                "hide": "0"
                                },
                            "notification_dl": {
                                "hide": "0"
                                },
                            "servicenow": {
                                "hide": "0"
                                },
                            "id": {
                                "hide": "0"
                                }
                            }
                        },
                    "hardware": {
                        "hide": "0",
                        "fields": {
                            "vendor_id": {
                                "hide": "0"
                                }
                            }
                        },
                    "oobinfo": {
                        "hide": "0"
                        },
                    "location_id": {
                        "hide": "0"
                        },
                    "ip": {
                        "hide": "0"
                        },
                    "id": {
                        "hide": "0"
                        }
                    },
                "searches": {
                        "and": {
                            "0": {
                                "type": "Any Part",
                                "negate": 0,
                                "key": "hostname",
                                "value": hostname
                                }
                            }
                        },
                "range": {
                        "from": 0,
                        "to": 0
                        },
                "format": "JSON",
                "relationships": "FRO"
              }
            }
    search_body = json.dumps( search_body )
    url = "https://esd.twcable.com/service/api/rest/search"
    r = requests.post( url, data={"json": search_body } )
    if r.json()['results'] == {}:
        return {}
    results = esdParseResults( r.json() )
    return esdFormatResults(results)

def esdContactSearch(appId):
    search_body = {
            "auth": {
                "eid": esd_eid,
                "api_key": esd_key
                },
            "call": {
                "table": "contact",
                "fields": {
                    "contactname": {
                        "hide": "0",
                        "sort": "1",
                        "sort_dir": "1"
                        },
                    "dl_contacts": {
                        "hide": "0",
                        "fields": {
                            "contactname": {
                                "hide": "0"
                                }
                            }
                        },
                    "application___appcontact": {
                        "hide": "0",
                        "fields": {
                            "id": {
                                "hide": "0"
                                }
                            }
                        }
                    },
                "searches": {
                    "and": {
                        "0": {
                            "type": "Any Part",
                            "negate": 0,
                            "key": "application___appcontact__id",
                            "value": appId
                            }
                        }
                    },
                "range": {
                    "from": 0,
                    "to": 0
                    },
                "format": "JSON",
                "relationships": "FRO"
                }
            }
    search_body = json.dumps( search_body )
    url = "https://esd.twcable.com/service/api/rest/search"
    r = requests.post( url, data={"json": search_body } )
    if r.json()['results'] == {}:
        return {}
    support = stripEmails( ', '.join( [ result['0'] for result in r.json()['results'].values() ] ) )
    dl_contacts = stripEmails( ', '.join( [ result['1'] for result in r.json()['results'].values() ] ) )
    return support, dl_contacts

def esdBulkHostnameSearch(hostnameArr):
    results = []
    threads = []
    def hostnameSearchThread(hostname):
        result = esdHostnameSearch(hostname)
        results.append(result)
    for hostname in hostnameArr:
        t = Thread(target=hostnameSearchThread, args=[hostname])
        threads.append(t)
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
    for result in results:
        if result == {}:
            results.remove(result)
    return results
