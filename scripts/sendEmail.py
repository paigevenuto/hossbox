import smtplib
from email.mime.text import MIMEtext

def sendEmail(recipients, emailSubject, emailBody, cc)
    server = smtplib.SMTP("mailrelay.chartercom.com")
    msg = MIMEtext(emailBody)
    sender = "hossbox@charter.com"

    recipients = [ "c-paige.venuto@charter.com" ]

    msg['Subject'] = emailSubject
    msg['From'] = sender
    msg['To'] = ', '.join(recipients)
    msg['CC'] = cc

    server.sendmail(sender, recipients, msg.as_string())
    server.quit()
