import requests
import json
import yaml

with open('../config.yml', 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)
oracle_VM_user = cfg['oracle_VM_user']
oracle_VM_pass = cfg['oracle_VM_pass']



def oracleCheckPoweredOn(ovmpmgr, hostname):
    url = f"https://{ovmpmgr}:7002/ovm/core/wsapi/rest"
    headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    r = requests.get(url+'/Vm', verify=False, auth=(oracle_VM_user, oracle_VM_pass), headers=headers)
    result = json.dumps( list( filter( lambda x: x['name'] == hostname, r.json() ) ) )[0]
    powerState = result['vmRunState']
    return powerState



url = f"https://{ovmpmgr}:7002/ovm/core/wsapi/rest"
headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
r = requests.get(url+'/Job', verify=False, auth=(oracle_VM_user, oracle_VM_pass), headers=headers)
result = r.json()
result = list( filter( lambda x: 'Stop' in x['description'], result) )
print(json.dumps(result, indent=4))

"""
example jobs
[
    {
        "id": {
            "type": "com.oracle.ovm.mgr.ws.model.Job",
            "value": "1633670487177",
            "uri": "https://ovmpmgr06:7002/ovm/core/wsapi/rest/Job/1633670487177",
            "name": "Stop VM: vm0pnoravcd0071"
        },
        "name": "Stop VM: vm0pnoravcd0071",
        "description": "Stop VM: vm0pnoravcd0071",
        "locked": false,
        "readOnly": false,
        "generation": 0,
        "userData": null,
        "resourceGroupIds": null,
        "resultId": null,
        "jobRunState": "SUCCESS",
        "jobSummaryState": "SUCCESS",
        "done": true,
        "summaryDone": true,
        "jobGroup": false,
        "error": {
            "message": null,
            "type": null
        },
        "progressMessage": null,
        "latestSummaryProgressMessage": null,
        "extraInfo": null,
        "parentJobId": null,
        "childJobIds": [],
        "user": "p2147479",
        "abortedByUser": null,
        "startTime": 1633670487214,
        "endTime": 1633670581862
    },
    {
        "id": {
            "type": "com.oracle.ovm.mgr.ws.model.Job",
            "value": "1633670129347",
            "uri": "https://ovmpmgr06:7002/ovm/core/wsapi/rest/Job/1633670129347",
            "name": "Stop VM: vm0pnoravcd0071"
        },
        "name": "Stop VM: vm0pnoravcd0071",
        "description": "Stop VM: vm0pnoravcd0071",
        "locked": false,
        "readOnly": false,
        "generation": 0,
        "userData": null,
        "resourceGroupIds": null,
        "resultId": null,
        "jobRunState": "SUCCESS",
        "jobSummaryState": "SUCCESS",
        "done": true,
        "summaryDone": true,
        "jobGroup": false,
        "error": {
            "message": null,
            "type": null
        },
        "progressMessage": null,
        "latestSummaryProgressMessage": null,
        "extraInfo": null,
        "parentJobId": null,
        "childJobIds": [],
        "user": "p2147479",
        "abortedByUser": null,
        "startTime": 1633670129387,
        "endTime": 1633670234722
    }
]
"""
