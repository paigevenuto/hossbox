import subprocess
import re
from threading import Thread

def serviceScan(hostname):
    results = subprocess.run(["nmap", hostname, "-sV"], capture_output=True)
    results = results.stdout.decode()
    results = results.split('\n')
    results = [ result for result in results if re.match( '\d*/... ', result ) ]
    results = [ result.split(' ') for result in results ]
    results = [ [item for item in result if item != '' ] for result in results]
    results = [ { 'port': result[0], 'state': result[1], 'service': result[2], 'version': ' '.join(result[3:]) } for result in results ]
    return {
            'hostname': hostname,
            'services': results
            }

"""
    Example output

    { 
    'hostname': 'exserver01',
    'services': [{'port': '22/tcp',
          'state': 'open',
          'service': 'ssh',
          'version': 'OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)'},
         {'port': '53/tcp',
          'state': 'open',
          'service': 'domain',
          'version': 'ISC BIND 9.16.1 (Ubuntu Linux)'},
         {'port': '80/tcp',
          'state': 'open',
          'service': 'http',
          'version': 'Apache httpd 2.4.41 ((Ubuntu))'}]
    }
"""

# def serviceScan(hostname):
    # return {
    # 'hostname': hostname,
    # 'services': [{'port': '22/tcp',
          # 'state': 'open',
          # 'service': 'ssh',
          # 'version': 'OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)'},
         # {'port': '53/tcp',
          # 'state': 'open',
          # 'service': 'domain',
          # 'version': 'ISC BIND 9.16.1 (Ubuntu Linux)'},
         # {'port': '80/tcp',
          # 'state': 'open',
          # 'service': 'http',
          # 'version': 'Apache httpd 2.4.41 ((Ubuntu))'}]
    # }

# TODO make this use concurrency
def batchServiceScan(hostnames):
    results = []
    threads = []

    for hostname in hostnames:
        t = Thread(target=serviceScan, args=[hostname])
        threads.append(t)
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
    return results

