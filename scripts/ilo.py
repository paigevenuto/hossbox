import hpilo
import time
import yaml
import sys
sys.path.insert(1, '../')

with open('config.yml', 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)
ilo_credslist = cfg['ilo_credslist']

def iloCheckPoweredOn(srvIp, creds):
    try:
        ilo = hpilo.Ilo(srvIp, creds['username'], creds['password'])
        power_status = ilo.get_host_power_status() 
        if power_status == 'ON':
            return True
        elif power_status == 'OFF':
            return False
    except :
        pass
    return 'incorrect IP address entered or unknown iLO credentials'

def iloCheckCreds(srvIp):
    for idx, pair in enumerate(ilo_credslist):
        try:
            time.sleep(50)
            ilo = hpilo.Ilo(srvIp, pair['username'], pair['password'])
            power_status = ilo.get_host_power_status() 
            return pair
        except :
            pass
    return 'incorrect IP address entered or unknown iLO credentials'

def iloShutdown(srvIp, creds):
    try:
        ilo = hpilo.Ilo(srvIp, creds['username'], creds['password'])
        shutdown = ilo.hold_pwr_btn(toggle=None)
        return shutdown
    except:
        pass
